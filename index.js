function composeMessage(to) {
    return `Hello ${to}`
}

function uncoveredFn() {
    const a = 1
    const b = 3
    return a + b
}

module.exports = {
    composeMessage,
    uncoveredFn
}