import re

# open the coverage file
with open("./coverage/cobertura-coverage.xml") as f: content = f.read()

# use a regex to extract the first instance of line-rate=""
pattern = r"line-rate=\"(\d?\.?\d+)\""
match = re.search(pattern, content)
str = match.group(1)

# convert to a percentage and print to stdout
coverage = float(str) * 100
print("Code Coverage:", coverage)
