const index = require('../index')

test ('Should compose a message', () => {
    expect(index.composeMessage('world')).toBe('Hello world')
})
